# house-of-miele

## Miele X challenge by Paulo Diogo
<paulo.diogo@codekultur.io>

In this challenge I was asked to:
```
Show the list of Game of Thrones characters with following functionalities:
- Pagination (max 10 per page)
- Filtering on different columns (text filter on the table)
- Sort ordering on different columns
- Use https://anapioficeandfire.com API to get the data
- Guess characters age using https://agify.io
```

The **anapioficeandfire** API presented some challenges to accomplish the requirements:
- It doesn't provide sort functionality, meaning that the frontend can only sort the characters already requested, but not through all characters available in the API
- Data availability is very inconsistent between characters (e.g. some have names, others not) and no unique Id is provided

Considering the sub-optimal sorting, I dicided to provide two different application modes to also showcase the possibility to sort between all characters:
- **App mode live:**
  - Characters are requested to the API per page
  - Filtering is done by the API
  - Sort is only available inside the current page carachters

- **App mode prefetch:**
  -  All characters are requested to the API at start
  - Filtering is done in the frontend
  - Sorting works between all characters

Apart from the different behaviour of sorting, all functionalities work in both modes, including enriching characters data with age, as long as the character has name and that name has age in agify.

Caching for all network requests is implemented via a third-party axios adapter. This improves performance and prevents duplicate unnecessary requests.
Age service features a inner-cache mechanism that prevents requesting ages for names already made available by prior requests.

Also a nice Lanister favicon is provided 🙂

## Tech
- Vue as JS frontend framework
- Vuex for state management
- Axios HTTP client
- Unit testing with Vue Test Utils with Jest
- Sass as CSS precompiler
- nanoid for unique Id's generation

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run unit tests
```
npm run test:unit
```

### Run and watch unit tests
```
npm run test:unit:watch
```

### Lints and fixes files
```
npm run lint
```
