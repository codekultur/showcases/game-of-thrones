import axiosInstance from '@/api/axiosInstance';
import { AxiosResponse } from 'axios';
import { Age } from '@/types/index';

const BASE_URL = 'https://api.agify.io';

const ageCache = {} as { [index: string]: number };

export const getAgesForNames = (namesList: string[] = []): Promise<Age[]> => {
  const missingAges = namesList.filter(name => Object.keys(ageCache).indexOf(name) === -1);

  if (missingAges.length) {
    const params = new URLSearchParams();

    missingAges.forEach(name => {
      params.append('name[]', name);
    });

    return axiosInstance.get(`${BASE_URL}?${params}`)
      .then((response: AxiosResponse) => {
        response.data.forEach((element: Age) => {
          ageCache[element.name] = element.age || 0;
        });

        return namesList.map((name: string) => {
          if (ageCache[name] !== undefined) {
            return {
              age: ageCache[name],
              name
            };
          } else {
            return {
              age: (response.data.find((age: Age) => age.name === name) || { age: 0 }).age,
              name
            };
          }
        });
      });
  }

  // Serve age information from cache
  return Promise.resolve(namesList.map(name => ({
    age: ageCache[name],
    name
  })));
};

export function getCacheObject (): { [index: string]: number } {
  return ageCache;
}
