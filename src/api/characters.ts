import { AxiosResponse } from 'axios';
import axiosInstance from '@/api/axiosInstance';
import { nanoid } from 'nanoid';
import { extractInformationFromHeader, getQueryParam } from '@/utils/helper';
import { Character, CharacterRaw, Filter } from '@/types';

const BASE_URL = 'https://www.anapioficeandfire.com/api';

export const getCharacters = (pageNumber = 1, pageSize = 10, activeFilters: Filter[] = []): Promise<{ characters: Character[], lastPage: number }> => {
  const configFilters: Record<string, string> = {};

  activeFilters.forEach(filter => {
    configFilters[filter.fieldName] = filter.value;
  });

  const axiosConfig = {
    params: {
      page: pageNumber,
      pageSize,
      ...configFilters
    }
  };

  return axiosInstance.get(`${BASE_URL}/characters`, axiosConfig)
    .then(async (response: AxiosResponse) => {
      return {
        characters: response.data.map((character: CharacterRaw) => ({
          aliases: character.aliases.join(', '),
          born: character.born,
          culture: character.culture,
          died: character.died,
          gender: character.gender,
          id: nanoid(),
          name: character.name,
          playedBy: character.playedBy.join(', ')
        })),
        lastPage: +getQueryParam(extractInformationFromHeader(response.headers.link, 'last'), 'page')
      };
    });
};
