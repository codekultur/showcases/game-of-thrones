import Vue from 'vue';
import Vuex from 'vuex';

// Store modules
import global from './modules/global';
import characters from './modules/characters';

Vue.use(Vuex);

const storeConfig = {
  modules: {
    global,
    characters
  }
};

const store = new Vuex.Store(storeConfig);
export default store;
