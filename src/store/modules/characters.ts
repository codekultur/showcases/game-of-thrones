import { getCharacters } from '@/api/characters';
import { getAgesForNames } from '@/api/age';
import { charactersCompareFunction } from '@/utils/helper';
import { Age, AppMode, Character, CharacterState, Filter, Pagination, SortOrder } from '@/types';

const SET_CHARACTERS = 'SET_CHARACTERS';
const SET_FILTER = 'SET_FILTER';
const SET_PAGINATION = 'SET_PAGINATION';
const SET_SORT = 'SET_SORT';
const RESET_CHARACTERS = 'RESET_CHARACTERS';
const RESET_PAGINATION = 'RESET_PAGINATION';
const REMOVE_FILTER = 'REMOVE_FILTER';
const RESET_SORT = 'RESET_SORT';

const defaultPageSize = 10;

export default {
  state: {
    activeFilters: [], // { fieldName: string, value: string }[]
    activeSort: {
      fieldName: '',
      order: SortOrder.descending
    },
    characters: [],
    pagination: {
      lastPage: 1,
      page: 1,
      pageSize: defaultPageSize
    }
  },

  getters: {
    /**
     * This getter returns the set of data to be shown in the data table.
     * @param {Object} state
     * @param _
     * @param {Object} rootState
     * @returns {Character[]}
     */
    activeCharacters (state: CharacterState, _: void, rootState: any): Character[] {
      if (rootState.global.appMode === AppMode.prefetch) {
        return state.characters
          .filter((character: Character) => {
            return state.activeFilters.every((filter: Filter) => {
              return character[filter.fieldName].toString().toLowerCase().indexOf(filter.value.toLowerCase()) > -1;
            });
          })
          .sort(charactersCompareFunction(state.activeSort.fieldName, state.activeSort.order))
          .slice((state.pagination.page - 1) * state.pagination.pageSize, state.pagination.page * state.pagination.pageSize);
      } else {
        return state.characters.sort(charactersCompareFunction(state.activeSort.fieldName, state.activeSort.order));
      }
    }
  },

  mutations: {
    [RESET_CHARACTERS]: (state: CharacterState): void => {
      state.activeFilters = [];
      state.characters = [];
    },

    [REMOVE_FILTER]: (state: CharacterState, fieldName: string): void => {
      state.activeFilters = state.activeFilters.filter((filter: Filter) => filter.fieldName !== fieldName);
    },

    [RESET_PAGINATION]: (state: CharacterState): void => {
      state.pagination = {
        lastPage: 1,
        page: 1,
        pageSize: defaultPageSize
      };
    },

    [RESET_SORT]: (state: CharacterState): void => {
      state.activeSort = {
        fieldName: '',
        order: SortOrder.descending
      };
    },

    [SET_SORT]: (state: CharacterState, fieldName: string): void => {
      if (state.activeSort.fieldName === '' || state.activeSort.fieldName !== fieldName) {
        state.activeSort = {
          fieldName,
          order: SortOrder.descending
        };
      } else {
        state.activeSort.order = state.activeSort.order === SortOrder.descending ? SortOrder.ascending : SortOrder.descending;
      }
    },

    [SET_FILTER]: (state: CharacterState, filter: Filter): void => {
      const filterIndex = state.activeFilters.findIndex(activeFilter => activeFilter.fieldName === filter.fieldName);

      if (filterIndex > -1) {
        state.activeFilters[filterIndex] = filter;
        return;
      }

      state.activeFilters.push(filter);
    },

    [SET_CHARACTERS]: (state: CharacterState, characters: Character[]): void => {
      state.characters = characters;
    },

    [SET_PAGINATION]: (state: CharacterState, pagination: Pagination): void => {
      state.pagination = pagination;
    }
  },

  actions: {
    /**
     * Returns all available character instances from the API.
     * @param {Object} store
     * @returns {Promise}
     */
    async getAllCharacters ({ commit, dispatch, state }: any): Promise<void> {
      if (state.characters.length > 0) {
        console.log('store::characters::getAllCharacters() -> characters have been fetched already');
        return;
      }

      dispatch('setIsLoading', true);

      try {
        const prefetchedCharacters = [] as Character[];
        const fetchPromises = [];

        const pageSize = 50; // Maximum allowed page size
        let count = 2; // Since we already have page 1, we must start from page 2

        // Get first page to receice total pages information
        const { characters, lastPage } = await getCharacters(1, pageSize);

        // Store first request data
        prefetchedCharacters.push(...characters);

        // Create requests for all pages
        while (count <= lastPage) {
          fetchPromises.push(getCharacters(count, pageSize));
          count++;
        }

        // Get all pages data and store it
        try {
          const results = await Promise.all(fetchPromises);
          results.forEach(result => prefetchedCharacters.push(...result.characters));
          commit(SET_CHARACTERS, prefetchedCharacters);
        } catch (error) {
          console.error('store::characters::getAllCharacters() -> error fetching all character', error);
          dispatch('setIsError', true);
        }
      } catch (error) {
        dispatch('setIsError', true);
      } finally {
        dispatch('setIsLoading', false);
        dispatch('setPagination', { page: 1 });
      }
    },

    /**
     * Returns a list of character instances from the API defined by the pagination object.
     * @param {Object} store
     * @param {Object} pagination
     * @returns {Promise}
     */
    async getCharacters ({ commit, dispatch, state }: any, { page, pageSize } = { page: 1, pageSize: defaultPageSize } as Pagination): Promise<void> {
      dispatch('setIsLoading', true);

      try {
        const { characters, lastPage } = await getCharacters(page, pageSize, state.activeFilters);

        commit(SET_CHARACTERS, characters);

        dispatch('setPagination', {
          lastPage,
          page,
          pageSize
        });

        dispatch('resetSort');
      } catch (error) {
        console.error('store::characters::getCharacters() -> error fetching live character', error);
        dispatch('setIsError', true);
      } finally {
        dispatch('setIsLoading', false);
      }
    },

    async loadAgeForCharacters ({ commit, state }: any, characters: Character[]): Promise<void> {
      if (!characters?.length) {
        return Promise.resolve();
      }

      const charactersFirstName = characters.filter(character => character.name).map(character => character.name.split(' ')[0]);

      try {
        const characterAges = await getAgesForNames(charactersFirstName);

        commit(SET_CHARACTERS, state.characters.map((character: Character) => {
          if (character.name) {
            const characterName = character.name.split(' ')[0];
            const characterAge = characterAges.find((age: Age) => age.name === characterName);

            if (characterAge) {
              character.age = characterAge.age as number;
            }
          }

          return character;
        }));
      } catch (error) {
        console.error('store::characters::getCharactersWithAge() -> error fetching live characters age', error);
      }
    },

    resetState ({ commit }: any): void {
      commit(RESET_CHARACTERS);
      commit(RESET_PAGINATION);
      commit(RESET_SORT);
    },

    removeFilter ({ commit, dispatch, rootState }: any, fieldName: string): void {
      commit(REMOVE_FILTER, fieldName);

      if (rootState.global.appMode === AppMode.live) {
        dispatch('getCharacters', {});
      }
    },

    resetPagination ({ commit }: any): void {
      commit(RESET_PAGINATION);
    },

    resetSort ({ commit }: any): void {
      commit(RESET_SORT);
    },

    setFilter ({ commit, dispatch, rootState }: any, filter: Filter): void {
      commit(SET_FILTER, filter);

      // When appMode is live, filtering and pagination is executed by the API
      if (rootState.global.appMode === AppMode.live) {
        dispatch('getCharacters');
      } else {
        // But when in prefetch mode, filtering is executed in the frontend
        // so we need to set the pagination to first page
        dispatch('setPagination', { page: 1 });
      }
    },

    /**
     * Sets the pagination and makes sure that the age information is loaded for all characters within that page.
     * @param {Object} store
     * @param {Object} pagination
     */
    setPagination ({ commit, dispatch, getters, rootState, state }: any, { lastPage = 1, page = 1, pageSize = defaultPageSize }: Pagination): void {
      // In prefetch mode, pagination needes to be calculated according to the active filters at the momment
      if (rootState.global.appMode === AppMode.live) {
        commit(SET_PAGINATION, {
          lastPage,
          page,
          pageSize
        });
      } else {
        commit(SET_PAGINATION, {
          lastPage: Math.ceil(state.characters
            .filter((character: Character) => {
              return state.activeFilters.every((filter: Filter) => {
                return character[filter.fieldName].toString().toLowerCase().indexOf(filter.value.toLowerCase()) > -1;
              });
            }).length / state.pagination.pageSize),
          page,
          pageSize
        });
      }

      dispatch('loadAgeForCharacters', getters.activeCharacters);
    },

    setSort ({ commit, dispatch }: any, fieldName: string): void {
      commit(SET_SORT, fieldName);

      // Set Pagination to first page
      dispatch('setPagination', { page: 1 });
    }
  }
};
