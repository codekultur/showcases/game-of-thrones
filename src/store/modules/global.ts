import { AppMode, GlobalState } from '../../types';

const SET_APP_MODE = 'SET_APP_MODE';
const SET_IS_LOADING = 'SET_IS_LOADING';
const SET_IS_ERROR = 'SET_IS_ERROR';

export default {
  state: {
    appMode: null,
    isLoading: false,
    isError: false
  },

  mutations: {
    [SET_APP_MODE]: (state: GlobalState, mode: AppMode): void => {
      state.appMode = mode;
    },

    [SET_IS_LOADING]: (state: GlobalState, isLoading: boolean): void => {
      state.isLoading = isLoading;
    },

    [SET_IS_ERROR]: (state: GlobalState, isError: boolean): void => {
      state.isError = isError;
    }
  },

  actions: {
    setAppMode ({ commit, dispatch }: any, mode: AppMode): void {
      commit(SET_APP_MODE, mode);

      dispatch('resetState');
    },

    setIsLoading ({ commit }: any, isLoading: boolean): void {
      commit(SET_IS_LOADING, isLoading);
    },

    setIsError ({ commit }: any, isError: boolean): void {
      commit(SET_IS_ERROR, isError);
    }
  }
};
