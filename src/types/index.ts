export enum AppMode {
  prefetch = 'prefetch',
  live = 'live'
}

export enum SortOrder {
  ascending = 'ascending',
  descending = 'descending'
}

export interface Age {
  age: number | null;
  name: string;
}

export interface Character {
  age: number;
  aliases: string;
  born: string;
  culture: string;
  died: string;
  gender: string;
  name: string;
  playedBy: string;

  [key: string]: string | number;
}

export interface CharacterRaw {
  aliases: string[];
  playedBy: string[];

  [key: string]: string | string[];
}

export interface CharacterState {
  activeFilters: Filter[];
  activeSort: Sort;
  characters: Character[];
  pagination: Pagination;
}

export interface GlobalState {
  appMode: AppMode | null;
  isLoading: boolean;
  isError: boolean;
}

export interface Filter {
  fieldName: string;
  value: string;
}

export interface Pagination {
  lastPage: number;
  page: number;
  pageSize: number;
}

export interface Sort {
  fieldName: string;
  order: SortOrder;
}
