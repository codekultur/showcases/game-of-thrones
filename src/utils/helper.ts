import { Character, SortOrder } from '@/types';

export const extractInformationFromHeader = (header: string, rel: string): string => {
  const headerArray = header.split(',').find(header => header.includes(`rel="${rel}"`));

  return (headerArray && headerArray.replace(`; rel="${rel}"`, '').replace(' ', '').replace('<', '').replace('>', '')) || '';
};

export const getQueryParam = (url: string, param: string): string => {
  const urlObj = new URL(url);

  return urlObj.searchParams.get(param) || '';
};

export const charactersCompareFunction = (fieldName: string, order: string) => (a: Character, b: Character): number => {
  if (fieldName !== '') {
    if (order === SortOrder.ascending) {
      return b[fieldName].toString().localeCompare(a[fieldName].toString());
    } else {
      return a[fieldName].toString().localeCompare(b[fieldName].toString());
    }
  }

  return 0;
};
