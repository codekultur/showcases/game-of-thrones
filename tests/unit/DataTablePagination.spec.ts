import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import DataDataTablePagination from '@/components/DataTable/DataDataTablePagination.vue';
import { AppMode } from '@/types';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('DataTablePagination component', () => {
  const actions: any = {
    getCharacters: jest.fn(),
    setPagination: jest.fn()
  };
  let wrapper: any;
  let state: any;
  let store: any;

  beforeEach(() => {
    state = {
      characters: {
        pagination: {
          lastPage: 3,
          page: 1,
          pageSize: 10
        }
      },
      global: {
        appMode: AppMode.live
      }
    };

    store = new Vuex.Store({ actions, state });

    wrapper = mount(DataDataTablePagination, {
      localVue,
      store
    });
  });

  describe('Initial state', () => {
    it('should start with isFirstPage true', () => {
      expect(wrapper.vm.isFirstPage).toBe(true);
    });

    it('should start with isFirstPage true', () => {
      expect(wrapper.vm.isFirstPage).toBe(true);
    });

    it('should start with isLastPage false', () => {
      expect(wrapper.vm.isLastPage).toBe(false);
    });

    it('should start with nextPageNumber = page + 1', () => {
      expect(wrapper.vm.nextPageNumber).toBe(2);
    });
  });

  describe('Template layout', () => {
    it('should render pagination div', () => {
      expect(wrapper.find('.pagination').exists()).toBe(true);
    });

    it('should render four navigation buttons', () => {
      expect(wrapper.findAllComponents({ name: 'app-button' })).toHaveLength(4);
    });

    it('should render element with current page number', () => {
      const paginationCurrent = wrapper.find('.pagination__current');

      expect(paginationCurrent.exists()).toBe(true);
      expect(paginationCurrent.text()).toBe('1');
    });
  });

  describe('Component API', () => {
    describe('First page button', () => {
      describe('On first page', () => {
        it('should not trigger handler', async () => {
          wrapper.vm.paginationPerNumber = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(0);
          await button.trigger('click');

          expect(wrapper.vm.paginationPerNumber).not.toHaveBeenCalled();
        });
      });

      describe('Not on first page', () => {
        it('should trigger handler', async () => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 2,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store
          });

          wrapper.vm.paginationPerNumber = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(0);
          await button.trigger('click');

          expect(wrapper.vm.paginationPerNumber).toHaveBeenCalled();
        });
      });
    });

    describe('Previous page button', () => {
      describe('On first page', () => {
        it('should not trigger handler', async () => {
          wrapper.vm.paginationPrev = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(1);
          await button.trigger('click');

          expect(wrapper.vm.paginationPrev).not.toHaveBeenCalled();
        });
      });

      describe('Not on first page', () => {
        it('should trigger handler', async () => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 2,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store
          });

          wrapper.vm.paginationPrev = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(1);
          await button.trigger('click');

          expect(wrapper.vm.paginationPrev).toHaveBeenCalled();
        });
      });
    });

    describe('Next page button', () => {
      describe('On last page', () => {
        it('should not trigger handler', async () => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 3,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.live
            }
          });

          wrapper.vm.paginationNext = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(2);
          await button.trigger('click');

          expect(wrapper.vm.paginationNext).not.toHaveBeenCalled();
        });
      });

      describe('Not on last page', () => {
        it('should trigger handler', async () => {
          wrapper.vm.paginationNext = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(2);
          await button.trigger('click');

          expect(wrapper.vm.paginationNext).toHaveBeenCalled();
        });
      });
    });

    describe('Last page button', () => {
      describe('On last page', () => {
        it('should not trigger handler', async () => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 3,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store
          });

          wrapper.vm.paginationPerNumber = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(3);
          await button.trigger('click');

          expect(wrapper.vm.paginationPerNumber).not.toHaveBeenCalled();
        });
      });

      describe('Not on last page', () => {
        it('should trigger handler', async () => {
          wrapper.vm.paginationPerNumber = jest.fn();
          const button = wrapper.findAllComponents({ name: 'app-button' }).at(3);
          await button.trigger('click');

          expect(wrapper.vm.paginationPerNumber).toHaveBeenCalled();
        });
      });
    });

    describe('Pagination methods', () => {
      describe('paginationNext()', () => {
        beforeEach(() => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 3,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.live
            }
          });
        });

        it('should trigger get characters when app mode is live', async () => {
          wrapper.vm.paginationNext();

          expect(actions.getCharacters).toHaveBeenCalled();
        });

        it('should trigger set pagination when app mode is prefetch', async () => {
          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.prefetch
            }
          });

          wrapper.vm.paginationNext();
          expect(actions.setPagination).toHaveBeenCalled();
        });
      });

      describe('paginationPrev()', () => {
        beforeEach(() => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 3,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.live
            }
          });
        });

        it('should trigger get characters when app mode is live', async () => {
          wrapper.vm.paginationPrev();

          expect(actions.getCharacters).toHaveBeenCalled();
        });

        it('should trigger set pagination when app mode is prefetch', async () => {
          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.prefetch
            }
          });

          wrapper.vm.paginationPrev();
          expect(actions.setPagination).toHaveBeenCalled();
        });
      });

      describe('paginationPerNumber()', () => {
        beforeEach(() => {
          state = {
            characters: {
              pagination: {
                lastPage: 3,
                page: 3,
                pageSize: 10
              }
            }
          };

          store = new Vuex.Store({ actions, state });

          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.live
            }
          });
        });

        it('should trigger get characters when app mode is live', async () => {
          wrapper.vm.paginationPerNumber(3);

          expect(actions.getCharacters).toHaveBeenCalled();
          expect(actions.getCharacters).toHaveBeenCalledWith(expect.any(Object), { page: 3 });
        });

        it('should trigger set pagination when app mode is prefetch', async () => {
          wrapper = mount(DataDataTablePagination, {
            localVue,
            store,
            computed: {
              appMode: () => AppMode.prefetch
            }
          });

          wrapper.vm.paginationPerNumber(3);
          expect(actions.setPagination).toHaveBeenCalled();
          expect(actions.setPagination).toHaveBeenCalledWith(expect.any(Object), { page: 3 });
        });
      });
    });
  });
});
