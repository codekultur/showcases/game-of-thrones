import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import ModeSelector from '@/components/ModeSelector.vue';
import { AppMode } from '@/types';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('ModeSelector component', () => {
  const actions: any = {
    getAllCharacters: jest.fn(),
    getCharacters: jest.fn(),
    setAppMode: jest.fn()
  };
  let wrapper: any;
  let store: any;

  beforeEach(() => {
    store = new Vuex.Store({ actions });

    wrapper = mount(ModeSelector, {
      localVue,
      store
    });
  });

  describe('Template layout', () => {
    it('should render two buttons', () => {
      expect(wrapper.findAllComponents({ name: 'app-button' })).toHaveLength(2);
    });
  });

  describe('Live Mode Selector', () => {
    it('should set the app mode to live', async () => {
      const button = wrapper.findAllComponents({ name: 'app-button' }).at(0);
      await button.trigger('click');

      expect(actions.setAppMode).toHaveBeenCalledWith(expect.any(Object), AppMode.live);
    });

    it('should trigger the store action to fetch the characters', async () => {
      const button = wrapper.findAllComponents({ name: 'app-button' }).at(0);
      await button.trigger('click');

      expect(actions.getCharacters).toHaveBeenCalled();
    });
  });

  describe('Prefetch Mode Selector', () => {
    it('should set the app mode to prefetch', async () => {
      const button = wrapper.findAllComponents({ name: 'app-button' }).at(1);
      await button.trigger('click');

      expect(actions.setAppMode).toHaveBeenCalledWith(expect.any(Object), AppMode.prefetch);
    });

    it('should trigger the store action to prefetch all characters', async () => {
      const button = wrapper.findAllComponents({ name: 'app-button' }).at(1);
      await button.trigger('click');

      expect(actions.getAllCharacters).toHaveBeenCalled();
    });
  });
});
