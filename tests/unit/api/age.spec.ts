import { getAgesForNames, getCacheObject } from '../../../src/api/age';
import axiosInstance from '../../../src/api/axiosInstance';

jest.mock('../../../src/api/axiosInstance', () => {
  return {
    get: jest.fn().mockReturnValue(Promise.resolve({
      data: [
        { name: 'Paulo', age: 41 },
        { name: 'Andre', age: 34 }
      ]
    }))
  };
});

describe('Age API', () => {
  it('should initialize an empty cache', () => {
    expect(getCacheObject()).toEqual({});
  });

  describe('getAgesForNames()', () => {
    it('should call the API with the right query string', () => {
      getAgesForNames(['Paulo', 'Andre']);

      expect(axiosInstance.get).toHaveBeenCalledWith('https://api.agify.io?name%5B%5D=Paulo&name%5B%5D=Andre');
    });

    it('should return the age object for each name', async () => {
      const results = await getAgesForNames(['Paulo', 'Andre']);

      expect(results).toEqual([
        { name: 'Paulo', age: 41 },
        { name: 'Andre', age: 34 }
      ]);
    });

    it('should update the cache object', () => {
      getAgesForNames(['Paulo', 'Andre']);

      expect(getCacheObject()).toEqual({
        Paulo: 41,
        Andre: 34
      });
    });

    it('should call the API with names not in cache only', () => {
      getAgesForNames(['Paulo', 'Andre']);

      getAgesForNames(['Paulo', 'Andre', 'Catarina']);

      expect(axiosInstance.get).toHaveBeenCalledWith('https://api.agify.io?name%5B%5D=Catarina');
    });
  });
});
